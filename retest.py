import re

text_to_search = '''Work Log
1. Work date (d/m/y)[today's date]

2. Start time-End time [8-16:30]

3. Break hours (.5)[0]

4. Customer [LA Construction]

5. Job Name

6. Descrition

7. Mileage (400-453)

8. Notes
10. test

12. test2'''

text2 = '1. help'
test3 = '12. test 12.'

# Raw string = string prefixed with "r" ex: r"this is a raw string"
# Need to be escaped: . ^ $ * + ? { } [] \ | () with a \

# \b match at a word boundary
# \d{1,2} match 1-2 numbers (0-9)
# \. match a period
# This finds our question numbers
pattern = re.compile(r'\b\d{1,2}\. ')

matches = pattern.finditer(text_to_search)

for match in matches:
	print(match)