'''Read user input from a text file, clear that input, and provide prompts
for the user'''

import os
import re
from itertools import cycle

class UserInputParser():

	# To prevent breaking code, add a key:string pair or
	# add to the "ordered prompts" list
	default_log_file_content = {
		'title': 'Work Log',
		'ordered prompts': [
			"1. Work date (d/m/y)[today's date]",
			"2. Start time-End time [8-16:30]",
			"3. Break hours (.5)[0]",
			"4. Customer [LA Construction]",
			"5. Job Name",
			"6. Description",
			"7. Mileage (400-453)",
			"8. Material or other charges? [n]"
			"9. Notes"
		]
	}

	default_receipt_file_content = {}

	def __init__(self):
		# This is where user input is found
		self.log_spreadsheet = None
		self.file_names = ['Work Log.txt', 'Work Log (1).txt', 'Work Log (2).txt']
		self.file_names_cycle = cycle(self.file_names)

		# Regular expression patterns
		# \b match at a word boundary
		# \d{1,2} match 1-2 numbers (0-9)
		# '\. ' match a period with a trailing space
		# '[\.\*] ' match a period or asterisk with a trailing space
		# This finds our question numbers
		self.required_question_pattern = re.compile(r'\b\d{1,2}\* ')
		self.not_required_question_pattern = re.compile(r'\b\d{1,2}\. ')
		self.question_pattern = re.compile(r'\b\d{1,2}[\.\*] ')
		self.blank_line_pattern = re.compile(r'^\s')
		self.num_0_99_pattern = re.compile(r'\d{1,2}')

		self._setLogFile()

		if self._fileHasInput() == True:
			print('File has input')
			self._hasRequiredAnswers()

		else:
			print('No input found')
			self._hasRequiredAnswers()


	def getUserInput(self):
		'''Get the user's input'''

	def _hasRequiredAnswers(self):
		'''Check that all required answers were provided and return True or
		   False'''
		with open(self.log_file, 'r') as f:
			lines = f.readlines()

		required_question = -1
		non_blank_lines_since_required_question = 0
		for i in range(
				len(self.default_log_file_content.keys()) - 1,
				len(lines)
			):
			line = lines[i]

			# Check for required and not required question patterns
			required_result = re.search(self.required_question_pattern, line)
			not_required_result = re.search(self.required_question_pattern, line)
			blank_line_result = re.search(self.blank_line_pattern, line)

			if not blank_line_result:
				print(f'Line {i+1} is not blank')


			elif required_result:
				question_num = re.search(
					self.num_0_99_pattern,
					pattern.group(0)
					).group(0)
				print(f'Question {question_num} on line {i+1} is required')
				required_question = question_num

			elif not_required_result:
				question_num = re.search(
					self.num_0_99_pattern,
					pattern.group(0)
					).group(0)
				print(f'Question {question_num} on line {i+1} isn\'t required')
				required_question = -1

			

	def _fileHasInput(self):
		'''Check if the user typed in the answer fields and return True or False'''
		with open(self.log_file, 'r') as f:
				lines = f.readlines()

		# Find the question patterns 
		for i in range(
				len(self.default_log_file_content.keys()) - 1,
				len(lines)
			):
			line = lines[i]

			# Look for the question pattern
			pattern = re.search(self.question_pattern, line)
			if pattern:
				question_num = re.search(
					self.num_0_99_pattern,
					pattern.group(0)
					).group(0)
				print(f'Found question {question_num} on line {i+1}!')

			# If a question pattern wasn't found, look for an answer
			else:
				if re.search(self.blank_line_pattern, line):
					print(f'Line {i+1} is empty')
				else:
					print(f'Question {question_num} has been answered on line {i+1}')
					return True

	def _setLogFile(self):
		'''Since Fast Notepad seems to rename files occasionally, lets make sure
		our note file exists'''
		self.user_path = os.path.expanduser('~')
		self.log_file_path = [
			'Documents',
			'Synced Documents',
			'Notes',
			'appnotes',
			'work'
		]

		exists = False
		loops = 0

		# Cycle through the file names to find an existing log file
		while exists == False and loops < 6:
			file = next(self.file_names_cycle)
			self.log_file = os.path.join(
					self.user_path,
					*self.log_file_path,
					file
				)

			if os.path.exists(self.log_file):
				# print(f'{file} exists!')
				exists = True
			else:
				# print(f'{file} doesn\'t exist')
				pass

	def _setPrompts(self):
		'''Set the prompts for the user to answer'''


if __name__ == '__main__':
	UserLogParser()